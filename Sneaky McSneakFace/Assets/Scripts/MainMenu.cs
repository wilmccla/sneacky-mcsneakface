﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public void Play() // Function that is called by the play button
    {
        SceneManager.LoadScene("Game");
    }

    public void Quit() // Function called by the quit button
    {
        Application.Quit();
    }
}
