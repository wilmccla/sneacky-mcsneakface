﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller {
    // Variables for Player Object Transform and NoiseMaker
	public Transform playerTransform;
    public Transform cameraTransform;
	public NoiseMaker noiseMaker;


    // Public MoveSpeed
	public float moveSpeed = 5;
    // Public TurnSpeed
	public float turnSpeed = 5;

    // How much sound turning and moving make
	public float moveVolume = 5;
	public float turnVolume = 5;

	// Declaring the keys as public, so designers can change the key bindings
	public KeyCode up = KeyCode.W;
	public KeyCode left = KeyCode.A;
	public KeyCode down = KeyCode.S;
	public KeyCode right = KeyCode.D;
	public KeyCode strafeLeft = KeyCode.Q;
	public KeyCode strafeRight = KeyCode.E;

	// Use this for initialization
	public override void Start () {
        base.Start();
		playerTransform = gameObject.transform;

		// Initialize a NoiseMaker
		noiseMaker = GetComponent<NoiseMaker>();
	}
	
	// Update is called once per frame
	void Update () {
        // Key Inputs
		if (Input.GetKey(up))
		{
			pawn.Move(playerTransform.right);
		}

		if (Input.GetKey(strafeRight))
		{
			pawn.Move(-playerTransform.up);
		}

		if (Input.GetKey(down))
		{
			pawn.Move(-playerTransform.right);
		}

		if (Input.GetKey(strafeLeft))
		{
			pawn.Move(playerTransform.up);
		}

		if (Input.GetKey(left))
		{
			pawn.Turn(true);
		}

		if (Input.GetKey(right))
		{
			pawn.Turn(false);
		}

        cameraTransform.position = new Vector3(playerTransform.transform.position.x, playerTransform.transform.position.y, Camera.main.transform.transform.position.z);
    }
}