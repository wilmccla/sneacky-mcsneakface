﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardPawn : Pawn {

    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        base.Update();
    }

    public override void Patrol()
    {
        Vector3 LookAtPoint = waypoints[point].position - GuardTransform.position;
        GuardTransform.right = LookAtPoint;

        Move(GuardTransform.right);


        if (Vector2.Distance(GuardTransform.position, waypoints[point].position) < 0.1f)
        {
            if (WaitTime <= 0 && point == 0)
            {
                point = 1;
                WaitTime = StartWaitTime;
            }

            if (WaitTime <= 0 && point == 1)
            {
                point = 0;
                WaitTime = StartWaitTime;
            }
            else
            {
                WaitTime -= Time.deltaTime;
            }
        }
    }

    public override void Move(Vector3 direction)
    {
        // Move in the direction passed in, at speed "moveSpeed"
        GuardTransform.position += (direction.normalized * GuardSpeed * Time.deltaTime);

        // Using the noisemaker to make noise while the player is moving
        if (noiseMaker != null)
        {
            noiseMaker.volume = Mathf.Max(noiseMaker.volume, moveVolume);
        }
    }
}
