﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// base class for the pawn
public abstract class Pawn : MonoBehaviour
{
    // --------PLAYER VARIABLES--------
    public Transform playerTransform;
    public Vector3 homePosition;
    public NoiseMaker noiseMaker;
    public float moveSpeed = 0.06f;
    public float moveVolume = 1f;
    public float turnSpeed = 2f;
    public float turnVolume = 1f;
    public Sprite sprite;

    // --------GUARD VARIABLES--------
    public Transform GuardTransform;
    public Transform[] waypoints;
    public int point = 0;

    public float GuardSpeed = 1f;
    public float WaitTime;
    public float StartWaitTime;

    public virtual void Start()
    {
        playerTransform = GetComponent<Transform>();
        GetComponent<SpriteRenderer>().sprite = sprite;

        homePosition = playerTransform.position;

        WaitTime = StartWaitTime;
    }

    public virtual void Update()
    {

    }

    public virtual void Move(Vector3 direction)
    {

    }

    public virtual void Turn(bool isTurnClockwise)
    {

    }

    public virtual void Patrol()
    {

    }

    public virtual void Chase()
    {

    }

    public virtual void Find()
    {

    }

    public virtual void Home()
    {

    }

    public virtual void Look(Vector3 target)
    {

    }
}
