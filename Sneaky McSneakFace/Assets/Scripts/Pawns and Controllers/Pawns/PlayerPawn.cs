﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPawn : Pawn
{
	// Use this for initialization
	public override void Start()
	{
		// Call the base Pawn class Start
		base.Start();
	}

	// Update is called once per frame
	public override void Update()
	{
		// Call the base Pawn class Update
		base.Update();
	}

    public override void Move(Vector3 direction)
    {
        // Move in the direction passed in, at speed "moveSpeed"
        playerTransform.position += (direction.normalized * moveSpeed);

        // Using the noisemaker to make noise while the player is moving
        if (noiseMaker != null)
        {
            noiseMaker.volume = Mathf.Max(noiseMaker.volume, moveVolume);
        }
    }

    public override void Turn(bool isTurnClockwise)
    {
        // Rotate based on turnSpeed and direction we are turning
        if (isTurnClockwise)
        {
            playerTransform.Rotate(0, 0, turnSpeed);

            // Using the noisemaker to make noise while the player is turning
            if (noiseMaker != null)
            {
                noiseMaker.volume = Mathf.Max(noiseMaker.volume, turnVolume);
            }
        }
        else
        {
            playerTransform.Rotate(0, 0, -turnSpeed);
            // Using the noisemaker to make noise while the player is turning
            if (noiseMaker != null)
            {
                noiseMaker.volume = Mathf.Max(noiseMaker.volume, turnVolume);
            }
        }
    }
}